class AddAttributesToEvent < ActiveRecord::Migration
  def change
    add_column :events, :facebook_id, :string
    add_column :users, :access_token, :string
    add_column :events, :owner_id, :integer
  end
end
