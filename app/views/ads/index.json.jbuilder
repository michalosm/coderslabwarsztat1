json.array!(@ads) do |ad|
  json.extract! ad, :title, :description, :location, :owner_id_id
  json.url ad_url(ad, format: :json)
end
